﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            DataTable dt = DBConnection.GetTables();
            
            
            comboBox1.DisplayMember = "TABLE_NAME";
            comboBox1.ValueMember = "TABLE_NAME";
            comboBox1.DataSource = dt;

            comboBox2.DisplayMember = "TABLE_NAME";
            comboBox2.ValueMember = "TABLE_NAME";
            comboBox2.DataSource = dt;

            comboBox3.DisplayMember = "TABLE_NAME";
            comboBox3.ValueMember = "TABLE_NAME";
            comboBox3.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string name = textBox1.Text;
            string pass = textBox2.Text;
            string table = comboBox2.SelectedValue.ToString();

            int add = DBConnection.AddUser(name, pass, table);
            if (add == 1)
            {
                MessageBox.Show("Successfully added");
                textBox1.Text = "";
                textBox2.Text = "";
            }
            //button1.Text = add.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = DBConnection.GetAll(comboBox1.SelectedValue.ToString());
            dataGridView1.DataSource = dt;
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = DBConnection.GetAll(comboBox3.SelectedValue.ToString());
            comboBox4.DisplayMember = "firstname";
            comboBox4.ValueMember = "id";
            comboBox4.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string id = comboBox4.SelectedValue.ToString();
            string table = comboBox3.SelectedValue.ToString();
            int del = DBConnection.DeleteUser(table, id);

            if (del==1)
            {
                MessageBox.Show("Successfully deleted");
                textBox1.Text = "";
                textBox2.Text = "";
            }
        }
    }
}
