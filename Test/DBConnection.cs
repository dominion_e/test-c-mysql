﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;

namespace Test
{
    class DBConnection
    {
        static string server = ConfigurationManager.AppSettings["server"].ToString();
        static string user = ConfigurationManager.AppSettings["user"].ToString();
        static string password = ConfigurationManager.AppSettings["password"].ToString();
        static string port = ConfigurationManager.AppSettings["port"].ToString();
        static string database = ConfigurationManager.AppSettings["database"].ToString();
        public static string GetConnection()
        {
            

            string conn = @"server=" + server + ";userid=" + user + ";database=" + database + ";port=" + port + ";password=" + password + ";";
           // string conn = @"server=localhost;userid=root;password=00000000;database=domi";
           // MySqlConnection conn = new MySqlConnection(connectionString);
            
            return conn;
        }
        
        public static int AddUser(string firstname, string lastname, string table)
        {
              string connectionString = GetConnection();
            
            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "INSERT INTO " + table + "(firstname, lastname) VALUES(@lastname, @lastname)";
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@firstname", firstname);
                cmd.Parameters.AddWithValue("@lastname", lastname);
                cmd.Parameters.AddWithValue("@table", table);

                cmd.ExecuteNonQuery();
                return 1;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }

        public static DataTable GetAll(string table)
        {
            string connectionString = GetConnection();
            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "select * from " +table;
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@table", table);

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = cmd;
                DataTable dTable = new DataTable();
                    adapter.Fill(dTable);
                    return dTable; 
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }

        public static DataTable GetTables()
        {
            string connectionString = GetConnection();
            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE table_type = 'base table'  AND table_schema='domi';";
                cmd.Prepare();

                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = cmd;
                DataTable dTable = new DataTable();
                adapter.Fill(dTable);
                return dTable; // here i have assign dTable object to the dataGridView1 object to display data.               
                // MyConn2.Close();  
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }

        public static int DeleteUser( string table, string id)
        {
            string connectionString = GetConnection();

            MySqlConnection connection = null;
            try
            {
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand cmd = new MySqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "DELETE FROM " + table + " WHERE id =@id";   
                cmd.Prepare();

                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                return 1;
            }
            finally
            {
                if (connection != null)
                    connection.Close();
            }

        }
    }

    
}
